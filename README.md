# Projet AirSim : Mise en place & explication du deep-learning



La version actuelle du deep-learning mise en place est une version de "test" se servant du modèle du SUV de AirSim.

Elle se base sur l'algorithme [*DQNcar.py*](https://github.com/Microsoft/AirSim/blob/master/PythonClient/DQNcar.py) proposé par [AirSim](https://github.com/Microsoft/AirSim) dans ses fichiers de tests avec des modifications de sorte à ne pas faire s'arrêter le SUV face à un obstacle mais à lui faire contourner ce dernier.



## Suite

L'objectif serait d'améliorer cet algorithme de sorte à le rendre totalement viable dans le cadre d'un évitement d'obstacles.

Il serait même sans doute intéressant une fois à l'aise avec le DL & avec les objectifs du projet de redévelopper l'algorithme de zéro entièrement obtimisé pour le projet actuel. 

L'algorithme actuel peut dans tout les cas servir de départ pour quiconque désire bien comprendre le fonctionnement de ce type d'algorithmes pour permettre sa réimplémentation par la suite de manière plus complète dans l'environnement.

## Prérequis

Cet algorithme se sert du framework [CNTK](https://github.com/Microsoft/CNTK) proposé par Microsoft.

Outre la mise en place de l'environnement de simulation et la mise en place d'AirSim les seuls prérequis est donc l'installation de CNTK ainsi que de python.

- [Installation de Python (pip) sur Windows](https://stackoverflow.com/questions/4750806/how-do-i-install-pip-on-windows)
- [Installation de CNTK](https://docs.microsoft.com/en-us/cognitive-toolkit/setup-windows-python?tabs=cntkpy22)

## Mise en place

Une fois la simulation AirSim mise en place et l'installation de tout les prérequis faite, le lancement de la phase d'entraînement est simple.

En effet il suffit de lancer la simulation sur Unreal via le bouton play ![Play](https://i.imgur.com/uvTKRWr.png) ensuite d'aller exécuter via python le fichier *DQNcar.py* (qui peut être remplacer par votre propre fichier, la seule condition étant qu'il se trouve dans le même dossier que *AirSimClient.py*)



Une fois ceci fait, l'algorithme va tourner aussi longtemps que nécessaire et procéder à l'entrainement du modèle.



***Attention, sous cette forme l'entraînement n'est fait que localement à la session. Il faudra modifier l'algorithme pour que l'entraînement soit enregistré***.

## Explication du code

La grande majorité du fichier est relativement générique, en effet, elle découle des exemples présents dans la documentation de CNTK.

Les spécifités commencent à partir de la fonction `interpret_action(action)`, cette dernière à pour but de définir les actions possibles par le véhicule.
Pour plus d'informations sur les actions proposée par AirSim, se référer à la documentation de ce dernier.

Ensuite la fonction `compute_reward(car_state)` a pour objectif, comme son nom l'indique, de calculer la récompenses du modèle.

La fonction `isDone(car_state, car_controls, reward)` quant à elle sert à définir quand est-ce que l'epoch actuel se termine. En l'occurence, lorsque le model rentre en collision.

Ensuite tout cela est assembler dans le code juste en dessous.
Rien de compliqué sur cette partie du code normalement.